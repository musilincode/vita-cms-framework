var Vue = require('vue');
var Main = require('com.vita.index/index')
var App = require('app.vue'); 
var VueRouter = require('vue-router');
var Router = require('router');

require('expose?$!expose?jQuery!jquery')
require('jquery-ui')
require('bootstrap')
require('nanoscroller')
require('uniform/jquery.uniform.js')
require('../node_modules/jquery-slimscroll/jquery.slimscroll')
require('./libs/jquery-ui.custom.min')
require('./libs/scripts')
require('./libs/pace.min')

Vue.use(VueRouter);

//注册路由
var router = new VueRouter({
    history: false,
    saveScrollPosition: true
});


Router(router, Main);

//启动 APP
// var App = Vue.extend({});
router.start(App, '#app');

window.router = router;