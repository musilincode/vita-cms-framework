var webpack = require('webpack');
var path = require('path');
var vue = require('vue-loader')

var resolveBowerPath = function(pluginName) {
    return path.join(__dirname, 'bower_components', pluginName, 'dist', pluginName, '.js');
};

var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {

  devtool: 'source-map', 
  // devtool: 'eval',

  entry: {
    entry: ['./src/entry'],
    // vendor: ['./src/vendor/zepto/zepto', './src/vendor/underscore/underscore'],
  },
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, './build/'),
    publicPath: '/static/'
  },
  module: {
    loaders: [{ 
      test: /\.scss$/, 
      loaders: ['style', 'css', 'autoprefixer', 'sass']
      // loader: ExtractTextPlugin.extract(
      //               // activate source maps via loader query
      //               'css-loader?sourceMap!' +
      //               'sass-loader?sourceMap'
      //           )
    }, 
    {
      test: /\.vue$/, 
      loader: 'vue'
      // loaders: {
      //   sass: 'style!css!autoprefixer!sass'
      // }
    }, 
    {
      test: /\.css$/,
      loaders: ['style', 'css']
    },
    {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'babel',
      query: {
        presets: ['es2015'],
        plugins: ['transform-runtime']
      }
    },
     {
      test: /\.(png|jpg)\?__inline$/,
      loader: 'url?limit=25000'
    }, {
      test: /\.(png|jpg)$/,
      loader: 'file'
    }
    ]
  },
  plugins: [
    // new webpack.ProvidePlugin({$: 'jQu'}),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    // new webpack.optimize.UglifyJsPlugin({
    //     compress: {
    //         warnings: false
    //     }
    // })
    new webpack.ResolverPlugin(
        new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
    ),
    new ExtractTextPlugin("[name].css")
    // new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js', Infinity)
  ],
  resolve: {
    root: [process.cwd() + '/node_modules', path.resolve('src'), path.join(__dirname, 'bower_components')],
    extensions: ['', '.js', '.css', '.scss', '.vue', '.png', '.jpg'],
    alias: {
      // jQuery: 'zepto/zepto.js'
    }
  },

  babel: {
    presets: ['es2015', 'stage-0'],
    plugins: ['transform-runtime']
  },

  //dev-serve
  devServer: {
    contentBase: "./src",
    publicPath: '/static/',
    noInfo: true, //  --no-info option
    hot: true,
    inline: true,
    colors: true,
    host: '0.0.0.0',
    port: 7788,
    historyApiFallback: true
  }


};